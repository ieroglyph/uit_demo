#ifndef INPUTFILEINFO_H
#define INPUTFILEINFO_H

#include <QSettings>
#include <QString>

/*!
 * \brief The InputFileInfo class
 * Class contains info describing input files and a method to read it from config.
 */
class InputFileInfo
{
public:
    InputFileInfo();

    //! Field containing client's name for corresponding column, representing this input file's recepient
    QString ClientName;

    //! Parameter containg a path for input file.
    QString FileName;

    //! Method for parsing settings
    void readSettings(QSettings* settings);
};

#endif // INPUTFILEINFO_H
