#ifndef OUTPUTFILEINFO_H
#define OUTPUTFILEINFO_H

#include <QSettings>
#include <QString>

/*!
 * \brief The OutputFileInfo class
 * Class contains info describing output files and a method to read it from config.
 */
class OutputFileInfo
{
public:
    OutputFileInfo();

    //! Parameter containg "WITH" section for output request.
    QString Query;

    //! Parameter containg a path for output file.
    QString FileName;

    //! Method for parsing settings
    void readSettings(QSettings* settings);
};

#endif // OUTPUTFILEINFO_H
