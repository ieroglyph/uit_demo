#ifndef IUTDBWORKER_H
#define IUTDBWORKER_H

#include <exception>
#include <QSqlDatabase>

#include "InputFileInfo.h"
#include "OutputFileInfo.h"

class IutDbException : public std::exception
{
};

/*!
 * \brief The IutDbWorker class
 * Class incapsulates all work with database.
 */
class IutDbWorker
{
    QSqlDatabase _db;
    bool _isValid{ true };

public:
    IutDbWorker();

    /*!
     * \brief updateInputFile
     * Method for adding info about single input file, replacing info about previous one.
     * \param input Input file info
     */
    void updateInputFile(InputFileInfo input);

    /*!
     * \brief formatOutputFile
     * Method to format out file
     * \param output Info describing request and path to ouput file
     */
    void formatOutputFile(OutputFileInfo output);

    /*!
     * \brief isValid
     * \return true if database was created in the right way
     */
    bool isValid();
};

#endif // IUTDBWORKER_H
