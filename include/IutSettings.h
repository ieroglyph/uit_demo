#ifndef IUTSETTINGS_H
#define IUTSETTINGS_H

#include <QString>
#include <QVector>

#include "InputFileInfo.h"
#include "OutputFileInfo.h"

/*!
 * \brief The IutSettings class
 * Class provides methods for parsing the settings file.
 */
class IutSettings
{
public:
    IutSettings(QString filename = QString());

    QVector<InputFileInfo> inputs;
    QVector<OutputFileInfo> outputs;

    InputFileInfo getInputByFilename(QString filename);
};

#endif // IUTSETTINGS_H
