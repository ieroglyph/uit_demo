#include "../include/InputFileInfo.h"

#include <QFileInfo>

namespace
{
const char* const ClientName = "ClientName";
const char* const FileName = "FileName";
} // namespace

InputFileInfo::InputFileInfo()
{
}

void InputFileInfo::readSettings(QSettings* settings)
{
    this->FileName = QFileInfo(settings->value(::FileName, FileName).toString()).absoluteFilePath();

    this->ClientName = settings->value(::ClientName, ClientName).toString();
}
