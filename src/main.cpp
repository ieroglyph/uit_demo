#include <iostream>
#include <QCoreApplication>
#include <QDateTime>
#include <QFileSystemWatcher>
#include <QThread>

#include "InputFileInfo.h"
#include "IutDbWorker.h"
#include "IutSettings.h"
#include "OutputFileInfo.h"

enum class ExitCode : int
{
    Ok = 0,
    ConfigInputsError,
    ConfigOutputsError,
    InputError,
    DatabaseError
};

int main(int argc, char* argv[]) try
{
    QCoreApplication a(argc, argv);

    system("echo off");
    system("chcp 65001");

    std::cout << "ApPlE wAtChEr started at "
              << QDateTime::currentDateTime().toString().toStdString() << std::endl;

    // Read settings
    IutSettings settings;

    if (settings.inputs.empty())
        return static_cast<int>(ExitCode::ConfigInputsError);

    if (settings.outputs.empty())
        return static_cast<int>(ExitCode::ConfigOutputsError);

    // Create database
    IutDbWorker dbworker;

    // Check if database was created successfuly
    if (!dbworker.isValid())
        return 1;

    // Object helping monitor filesystem changes made to files
    QFileSystemWatcher watcher;

    // Thread to run watcher in
    QThread fsthread;

    // Import all input files
    std::cout << "Watching " << settings.inputs.size() << " files:" << std::endl;
    for (auto& input : settings.inputs)
    {
        dbworker.updateInputFile(input);
        watcher.addPath(input.FileName);
        std::cout << "\t" << input.FileName.toStdString() << std::endl;
    }

    if (watcher.files().isEmpty())
        return static_cast<int>(ExitCode::InputError);

    // Process all output files

    std::cout << "Output files: " << std::endl;
    for (auto& output : settings.outputs)
    {
        dbworker.formatOutputFile(output);
        std::cout << "\t" << output.FileName.toStdString() << std::endl;
    }

    // dbworker of configured, it's time to move it to its own thread
    watcher.moveToThread(&fsthread);
    fsthread.start();

    // Update outputs after watcher finds that something changed
    QObject::connect(&watcher, &QFileSystemWatcher::fileChanged, &a,
                     [&](const QString& filename) {
                         std::cout << "[" << QDateTime::currentDateTime().toString().toStdString()
                                   << "] file updated: " << filename.toStdString() << std::endl;
                         dbworker.updateInputFile(settings.getInputByFilename(filename));
                         // Process all output files
                         for (auto& output : settings.outputs)
                         {
                             dbworker.formatOutputFile(output);
                         }
                     },
                     Qt::QueuedConnection);

    QObject::connect(&a, &QCoreApplication::aboutToQuit, &fsthread, &QThread::quit,
                     Qt::QueuedConnection);

    return a.exec();
}
catch (IutDbException& e)
{
    return static_cast<int>(ExitCode::DatabaseError);
}
