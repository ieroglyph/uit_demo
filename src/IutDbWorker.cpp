#include "../include/IutDbWorker.h"

#include <QFile>
#include <QSqlError>
#include <QSqlQuery>
#include <QTextStream>

namespace
{
const char* const DatabaseType = "QSQLITE";
const char* const DatabaseName = ":memory:";
const char* const QueryCreateTable = "CREATE TABLE orders (CLIENT TEXT, FRUIT TEXT, PRICE NUMERIC, "
                                     "VENDOR TEXT, AMOUNT NUMERIC)";
const char* const QueryClearTable = "DELETE FROM orders WHERE CLIENT='%1'";
const char* const QueryInsertTable = "INSERT INTO orders VALUES('%1', '%2', %3, '%4', %5)";
const char* const QuerySelectTable = "SELECT CLIENT, FRUIT, PRICE, VENDOR, sum(AMOUNT) FROM "
                                     "orders WHERE %1 GROUP BY CLIENT, FRUIT, PRICE, VENDOR";
const char* const Codec = "UTF-8";
} // namespace

IutDbWorker::IutDbWorker()
{
    _db = QSqlDatabase::addDatabase(::DatabaseType);
    _db.setDatabaseName(::DatabaseName);
    _db.open();

    _db.exec(::QueryCreateTable);

    if (_db.lastError().type() != QSqlError::ErrorType::NoError)
    {
        _isValid = false;
    }
}

void IutDbWorker::updateInputFile(InputFileInfo input)
{
    QFile file(input.FileName);
    file.open(QIODevice::ReadOnly);

    QTextStream str(&file);
    str.setCodec(::Codec);

    QStringList lines;

    while (!str.atEnd())
        lines << str.readLine();

    _db.exec(QString(::QueryClearTable).arg(input.ClientName));

    if (_db.lastError().type() != QSqlError::ErrorType::NoError)
    {
        throw IutDbException();
    }

    for (auto& line : lines)
    {
        QStringList values = line.split(",");
        if (values.size() != 4)
            continue;
        _db.exec(QString(::QueryInsertTable)
                     .arg(input.ClientName)
                     .arg(values[0])
                     .arg(values[1])
                     .arg(values[2])
                     .arg(values[3]));

        if (_db.lastError().type() != QSqlError::ErrorType::NoError)
        {
            throw IutDbException();
        }
    }
}

void IutDbWorker::formatOutputFile(OutputFileInfo output)
{
    QFile file(output.FileName);
    file.open(QIODevice::WriteOnly);
    QTextStream ostr(&file);
    ostr.setCodec(::Codec);

    QSqlQuery query(_db);

    QString queryString = QString(::QuerySelectTable).arg(output.Query);

    query.exec(queryString);

    if (_db.lastError().type() != QSqlError::ErrorType::NoError)
    {
        throw IutDbException();
    }

    while (query.next())
    {
        ostr << query.value(0).toString() << "," << query.value(1).toString() << ","
             << query.value(2).toString() << "," << query.value(3).toString() << ","
             << query.value(4).toString() << "\n";
    }
}

bool IutDbWorker::isValid()
{
    return _isValid;
}
