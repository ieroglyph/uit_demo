#include "../include/IutSettings.h"

#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QSettings>

namespace
{
const char* const DefaultSettingsFilename = "config.ini";
const char* const SettingsInputs = "Inputs";
const char* const SettingsOutputs = "Outputs";
} // namespace

IutSettings::IutSettings(QString filename)
{
    if (filename == QString())
        filename = QDir(qApp->applicationDirPath()).filePath(::DefaultSettingsFilename);

    QSettings settings(filename, QSettings::IniFormat);
    settings.setIniCodec("UTF-8");

    // Reading all the input files
    settings.beginGroup(::SettingsInputs);
    int inputsCount = settings.beginReadArray(::SettingsInputs);
    for (int i = 0; i < inputsCount; i++)
    {
        settings.setArrayIndex(i);
        InputFileInfo input;
        input.readSettings(&settings);
        inputs.push_back(std::move(input));
    }
    settings.endArray();
    settings.endGroup();

    // Reading all the output files
    settings.beginGroup(::SettingsOutputs);
    int outputsCount = settings.beginReadArray(::SettingsOutputs);
    for (int i = 0; i < outputsCount; i++)
    {
        settings.setArrayIndex(i);
        OutputFileInfo output;
        output.readSettings(&settings);
        outputs.push_back(std::move(output));
    }
    settings.endArray();
    settings.endGroup();
}

InputFileInfo IutSettings::getInputByFilename(QString filename)
{
    for (const auto& input : inputs)
    {
        if (QFileInfo(input.FileName).canonicalFilePath() == QFileInfo(filename).canonicalFilePath())
            return input;
    }
    return InputFileInfo();
}
