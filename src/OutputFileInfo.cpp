#include "../include/OutputFileInfo.h"

#include <QFileInfo>

namespace
{
const char* Query = "Query";
const char* const FileName = "Filename";
} // namespace

OutputFileInfo::OutputFileInfo()
{
}

void OutputFileInfo::readSettings(QSettings* settings)
{
    // works with both absolute and relative paths
    this->FileName = QFileInfo(settings->value(::FileName, FileName).toString()).absoluteFilePath();
    // we have to perform sane validity checks here for this file in real application
    this->Query = settings->value(::Query, Query).toString();
}
